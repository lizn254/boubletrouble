package objects;


import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import models.GameModel;
import objects.weapons.Weapon;

import java.util.Random;

public class Ball extends MovingGameObject {
	
	//private GameModel model = GameModel.getInstance();
	private float ballSpeedX = 8;
	private float ballSpeedY = ballSpeedX * 1.4f;
	private static final double SCALE=0.8;
	private Circle ball;
	
	private static final double BALL_DIAMETER = ( float ) ( GameModel.getInstance ( ).getScreenSize ( ).width * 0.025 );
	
	{
		ball = new Circle ( BALL_DIAMETER );
		ball.setFill ( Color.RED );
		this.getChildren ( ).addAll ( ball );
	}
	
	// initialize speed
	{
		super.speedX = ballSpeedX;
		super.speedY = ballSpeedY;
	}
	
	
	public Ball ( Point2D position ) {
		super ( position );
	}
	
	@Override
	public void updatePosition ( ) {
		handleCollisions ( );
		position = new Point2D ( position.getX ( ) + speedX, position.getY ( ) + speedY );
		setTranslateX ( getTranslateX ( ) + speedX );
		setTranslateY ( getTranslateY ( ) + speedY );
		
		if ( speedY < 0 ) {
			speedY += 0.17;
		} else {
			speedY = ballSpeedY;
		}
	}
	
	@Override
	protected void handleCollisions ( ) {
		handleBorderCollisions ( );
		handlePlayerCollisions ( );
		handleBulletCollisions ( );
	}
	
	private void handleBulletCollisions ( ) {
		if ( GameModel.getInstance ( ).getWeapon ( ) == null ) {
			return;
		}
		if ( this.getBoundsInParent ( ) .intersects ( GameModel.getInstance ( ).getWeapon ( ).getBoundsInParent ( ) ) ) {
			GameModel.getInstance ( ).getRoot ( ).getChildren ( ).remove ( this );
			//GameModel.getInstance ( ).setGameWon ( true );
			GameModel.getInstance().getBalls().remove(this);

			Weapon w=GameModel.getInstance().getWeapon();
			GameModel.getInstance().setWeapon(null);
			GameModel.getInstance().getRoot().getChildren().remove(w);

			Ball b1=new Ball(new Point2D(this.position.getX(),this.position.getY()));
			Ball b2=new Ball(new Point2D(this.position.getX(),this.position.getY()));

			b1.ball.setRadius(this.ball.getRadius()*SCALE);
			b2.ball.setRadius(this.ball.getRadius()*SCALE);

			b1.speedX=this.speedX;
			b1.speedY=this.speedY;

			b2.speedX=-this.speedX;
			b2.speedY=this.speedY;

			Random r=new Random();
			Color c=Color.rgb((int)(r.nextDouble()*255),(int)(r.nextDouble()*255),(int)(r.nextDouble()*255));
			b1.ball.setFill(c);
			b2.ball.setFill(c);

			GameModel.getInstance().getRoot().getChildren().addAll(b1,b2);
			GameModel.getInstance().getBalls().add(b1);
			GameModel.getInstance().getBalls().add(b2);

		}
	}
	
	private void handlePlayerCollisions ( ) {
		if ( this.getBoundsInParent ( ).intersects ( GameModel.getInstance ( ).getPlayer ( ).getBoundsInParent ( ) ) ) {
			GameModel.getInstance ( ).setGameLost ( true );
		}
	}
	
	private void handleBorderCollisions ( ) {
		if ( position.getX ( ) - BALL_DIAMETER < 0 || position.getX ( ) > GameModel.getInstance ( ).getSceneWidth ( ) - BALL_DIAMETER ) {
			speedX = -speedX;
		}
		
		if ( position.getY ( ) - BALL_DIAMETER < 0 || position.getY ( ) > GameModel.getInstance ( ).getSceneHeight ( ) - BALL_DIAMETER ) {
			speedY = -speedY;
			//speedY = Math.signum(speedY) * GameModel.getInstance().ballSpeedY;
		}
	}
}
