package objects;

import javafx.geometry.Point2D;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import models.GameModel;
import playerStates.MovingLeftState;
import playerStates.MovingRightState;
import playerStates.StandingState;
import playerStates.State;

public class Player extends MovingGameObject {
	//private GameModel model = GameModel.getInstance();
	private             State state         = new StandingState ( this );
	public static final float PLAYER_WIDTH  = ( float ) ( GameModel.getInstance ( ).getScreenSize ( ).width * 0.01 );
	public static final float PLAYER_HEIGHT = 2 * PLAYER_WIDTH;
	public static final float PLAYER_SPEED  = 10;
	
	{
		// this is necessary in order for this class to respond to key events
		this.setFocusTraversable ( true );
	}
	
	{
		speedX = PLAYER_SPEED;
		speedY = 0;
		Rectangle player = new Rectangle ( PLAYER_WIDTH, PLAYER_HEIGHT );
		player.setFill ( Color.LIGHTBLUE );
		this.getChildren ( ).addAll ( player );
		
		this.addEventFilter ( KeyEvent.KEY_PRESSED, event -> {
			switch ( event.getCode ( ) ) {
				case RIGHT:
					state = new MovingRightState ( GameModel.getInstance ( ).getPlayer ( ) );
					break;
				case LEFT:
					state = new MovingLeftState ( GameModel.getInstance ( ).getPlayer ( ) );
					break;
			}
		} );
		
		this.addEventFilter (
				KeyEvent.KEY_RELEASED, event -> {
					if ( event.getCode ( ) == KeyCode.LEFT || event.getCode ( ) == KeyCode.RIGHT ) {
						state = new StandingState ( GameModel.getInstance ( ).getPlayer ( ) );
					}
				}
		);
	}
	
	public Player ( Point2D position ) {
		super ( position );
	}
	
	@Override
	protected void handleCollisions ( ) {
		if ( position.getX ( ) < 0 || position.getX ( ) > GameModel.getInstance ( ).getSceneWidth ( ) - PLAYER_WIDTH ) {
			state = new StandingState ( this );
			if ( position.getX ( ) < 0 ) {
				setPosition ( new Point2D ( 0, getPosition ( ).getY ( ) ) );
				setTranslateX ( 0 );
			}
			
			if ( position.getX ( ) > GameModel.getInstance ( ).getSceneWidth ( ) - PLAYER_WIDTH ) {
				setPosition ( new Point2D ( GameModel.getInstance ( ).getSceneWidth ( ) - PLAYER_WIDTH, getPosition ( ).getY ( ) ) );
				setTranslateX ( GameModel.getInstance ( ).getSceneWidth ( ) - PLAYER_WIDTH );
			}
		}
	}
	
	@Override
	public void updatePosition ( ) {
		state.update ( );
		handleCollisions ( );
	}
}
