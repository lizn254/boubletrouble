package main;

import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.geometry.Point2D;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import models.GameModel;
import objects.Background;
import objects.Ball;
import objects.Player;
import objects.weapons.Bullet;

import static objects.Player.PLAYER_HEIGHT;
import static objects.Player.PLAYER_WIDTH;

public class Main extends Application {
	
	private AnimationTimer timer;
	
	@Override
	public void start ( Stage primaryStage ) throws Exception {
		Group root = new Group ( );
		primaryStage.setTitle ( "Deoba mehura" );
		Scene scene = new Scene ( root, GameModel.getInstance ( ).getSceneWidth ( ), GameModel.getInstance ( ).getSceneHeight ( ) );
		primaryStage.setScene ( scene );
		
		// disable resizing and maximize button
		primaryStage.setResizable ( false );
		primaryStage.sizeToScene ( );
		
		Ball ball = new Ball ( new Point2D ( 100, 100 ) );
		GameModel.getInstance ( ).getBalls ( ).add ( ball );
		
		Player player = new Player ( new Point2D ( 100, GameModel.getInstance ( ).getSceneHeight ( ) - PLAYER_HEIGHT ) );
		GameModel.getInstance ( ).setPlayer ( player );
		
		root.getChildren ( ).addAll ( new Background ( ), ball, player );

		GameModel.getInstance ( ).setRoot ( root );
		
		scene.setOnKeyPressed ( event -> {
			if ( event.getCode ( ) == KeyCode.SPACE ) {
				Bullet bullet = new Bullet ( player.getPosition ( ) .add ( 0.5 * PLAYER_WIDTH, 0 ) );
				root.getChildren ( ).remove ( GameModel.getInstance ( ).getWeapon ( ) );
				GameModel.getInstance ( ).setWeapon ( bullet );
				root.getChildren ( ).addAll ( bullet );
			}
		} );
		primaryStage.show ( );
		
		timer = new AnimationTimer ( ) {
			@Override
			public void handle ( long l ) {
				if ( GameModel.getInstance ( ).isGameLost ( ) || GameModel.getInstance ( ).isGameWon ( ) ) {
					timer.stop ( );
				}
				for ( Ball ball : GameModel.getInstance ( ).getBalls ( ) ) {
					ball.updatePosition ( );
				}
				
				GameModel.getInstance ( ).getPlayer ( ).updatePosition ( );
				if ( GameModel.getInstance ( ).getWeapon ( ) != null ) {
					GameModel.getInstance ( ).getWeapon ( ).updatePosition ( );
				}
				
			}
		};
		timer.start ( );
	}
	
	public static void main ( String[] args ) {
		launch ( args );
	}
}
