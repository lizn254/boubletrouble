package models;

import javafx.scene.Group;
import objects.Ball;
import objects.Player;
import objects.weapons.Weapon;

import java.awt.*;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameModel {
    private Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
    private final double SCENE_SCALE_FACTOR = 0.7;
    private float sceneWidth = (float) (screenSize.getWidth() * SCENE_SCALE_FACTOR);
    private float sceneHeight = (float) (screenSize.getHeight() * SCENE_SCALE_FACTOR);
    
    private static GameModel thisInstance = null;

    private CopyOnWriteArrayList<Ball> balls = new CopyOnWriteArrayList<>();
    private Player player;
    private Weapon weapon;
    private boolean gameLost;
    private boolean gameWon;
    private Group root;

    public static GameModel getInstance() {
        if (thisInstance == null) {
            thisInstance = new GameModel();
        }
        return thisInstance;
    }

    public float getSceneWidth() {
        return sceneWidth;
    }

    public float getSceneHeight() {
        return sceneHeight;
    }

    public double getScreenWidth() {
        return screenSize.getWidth();
    }

    public double getScreenHeight() {
        return screenSize.getHeight();
    }

    public void setGameLost(boolean gameLost) {
        this.gameLost = gameLost;
    }

    public boolean isGameLost() {
        return gameLost;
    }

    public CopyOnWriteArrayList<Ball> getBalls() {
        return balls;
    }

    public void setBalls(CopyOnWriteArrayList<Ball> balls) {
        this.balls = balls;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public Group getRoot() {
        return root;
    }

    public void setRoot(Group root) {
        this.root = root;
    }

    public boolean isGameWon() {
        return gameWon;
    }

    public void setGameWon(boolean gameWon) {
        this.gameWon = gameWon;
    }

    public Dimension getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Dimension screenSize) {
        this.screenSize = screenSize;
    }
}
